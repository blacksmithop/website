from flask import render_template
from flask import Flask
from os import urandom
from blueprints import authentication, apiroutes
from dotenv import load_dotenv


# Initialize the app with random secret_key
app = Flask(__name__)
app.secret_key = urandom(12)

# Load Environment Variables
load_dotenv()

# Add Blueprints
app.register_blueprint(authentication.auth)
app.register_blueprint(apiroutes.api)


@app.route("/")
def index():
    return render_template("index.html")


@app.errorhandler(Exception)
def exception_handler(error):
    return render_template('error-pages/error.html', code=error.code,
                           name=error.name, message=error.description), 404


if __name__ == '__main__':
    app.run(debug=False)

